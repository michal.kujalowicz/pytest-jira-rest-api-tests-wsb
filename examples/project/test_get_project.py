import pytest
import requests
from common import config

PROJECT_REST_URL = config.JIRA_TEST_INSTANCE_URL + "/rest/api/2/project"


def test_get_all_projects_unauthorised_no_project_returned():
    """requesting for all projects when unauthorised and no project is returned"""
    response = requests.get(PROJECT_REST_URL)
    assert response.status_code == 200
    response_body = response.json()
    assert len(response_body) == 0


def test_get_all_projects_authorised_successful():
    """requesting for all projects when authorised and 1 project is returned"""
    response = requests.get(PROJECT_REST_URL, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 200
    response_body = response.json()
    assert len(response_body) == 1
    assert response_body[0]['key'] == config.PROJECT_WRITE_PERMISSION_ID


def test_get_project_authorised_successful():
    """requesting for specific project when authorised and 1 project is returned"""
    project_url = PROJECT_REST_URL + "/" + config.PROJECT_WRITE_PERMISSION_ID
    response = requests.get(project_url, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 200
    response_body = response.json()
    assert response_body['key'] == config.PROJECT_WRITE_PERMISSION_ID
    assert response_body['lead']['name'] == config.JIRA_USER


def test_get_project_no_permission():
    """requesting for specific project and no permission"""
    project_url = PROJECT_REST_URL + "/" + config.PROJECT_NO_PERMISSION
    response = requests.get(project_url, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 404
    response_body = response.json()
    assert response_body['errorMessages'][0] == config.PROJECT_NOT_FOUND_ERROR_MESSAGE

