import pytest
import requests
import json
from common import credentials
from common import helpers


def test_edit_issue_edit_summary(test_issue):
    """editing summary of issue I have permission to"""
    summary = "New summary"
    data = json.dumps({
        "fields": {
            "summary": summary
        }
    })
    response = requests.put(credentials.ISSUE_REST_URL + "/" + test_issue, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                            headers={'content-type': 'application/json'})
    assert response.status_code == 204
    assert response.text == ''
    """ Checking if summary is actually changed"""
    response = requests.get(credentials.ISSUE_REST_URL + "/" + test_issue, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    response_body = response.json()
    actual_summary = response_body['fields']['summary']
    assert actual_summary == summary


def test_edit_issue_add_label(test_issue):
    """adding label to issue I have permission to"""
    label = "newlabel"
    data = json.dumps({
        "update": {
            "labels": [
                {"add": label}
            ]
        }
    })
    response = requests.put(credentials.ISSUE_REST_URL + "/" + test_issue, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                            headers={'content-type': 'application/json'})
    assert response.status_code == 204
    assert response.text == ''
    """ Checking if label is actually changed"""
    response = requests.get(credentials.ISSUE_REST_URL + "/" + test_issue, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    response_body = response.json()
    labels = response_body['fields']['labels']
    assert label in labels
