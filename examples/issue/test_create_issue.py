import pytest
import requests
import json
from common import credentials
from common import helpers


def test_create_issue():
    """creating issue in project I have permission to"""
    project_id = helpers.get_project_id(credentials.PROJECT_WRITE_PERMISSION_ID)
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": "This is a test issue",
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 201
    response_body = response.json()

    """ Cleanup """
    issue_key = response_body['key']
    helpers.delete_issue(issue_key)


def test_create_issue_unauthorised():
    """creating issue when unauthorised"""
    project_id = helpers.get_project_id(credentials.PROJECT_WRITE_PERMISSION_ID)
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": "This is a test issue",
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, "bla"),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 401


def test_get_issue_no_permission():
    """creating issue in project I do not have permission to"""
    project_id = credentials.PROJECT_NO_PERMISSION_ID
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": "This is a test issue",
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 400


def test_create_issue_no_project():
    """creating issue but no project provided"""
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "summary": "This is a test issue",
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 400
    response_body = response.json()
    error_message = response_body['errors']['project']
    assert error_message == "project is required"


def test_create_issue_no_summary():
    """creating issue but no summary provided"""
    project_id = helpers.get_project_id(credentials.PROJECT_WRITE_PERMISSION_ID)
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 400
    response_body = response.json()
    error_message = response_body['errors']['summary']
    assert error_message == "You must specify a summary of the issue."


def test_create_issue_no_issuetype():
    """creating issue but no issuetype provided"""
    project_id = helpers.get_project_id(credentials.PROJECT_WRITE_PERMISSION_ID)
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": "This is a test issue",
            "description": "This is a test issue",
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 400
    response_body = response.json()
    error_message = response_body['errors']['issuetype']
    assert error_message == "issue type is required"

