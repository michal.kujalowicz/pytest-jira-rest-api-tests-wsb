import pytest
import requests
import json
from common import credentials
from common import helpers


def test_delete_issue_successful(test_issue_to_delete):
    """deleting issue I have permission to"""
    response = requests.delete(credentials.ISSUE_REST_URL + "/" + test_issue_to_delete, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    assert response.status_code == 204
    assert response.text == ''
