import pytest
import requests
from common import credentials


def test_get_issue_successful(test_issue):
    """requesting for issue which exists and I have permission to"""
    response = requests.get(credentials.ISSUE_REST_URL + "/" + test_issue, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    assert response.status_code == 200


def test_get_issue_unauthorised(test_issue):
    """requesting for issue which exists unauthorised"""
    response = requests.get(credentials.ISSUE_REST_URL + "/" + test_issue)
    assert response.status_code == 401


def test_get_issue_no_permission():
    """requesting for issue to which I do not have permission to"""
    response = requests.get(credentials.ISSUE_REST_URL + "/" + credentials.PROJECT_NO_PERMISSION + "-1",
                            auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    assert response.status_code == 403


def test_get_issue_issue_not_existing():
    """requesting for issue which does not exist"""
    response = requests.get(credentials.ISSUE_REST_URL + "/" + credentials.PROJECT_WRITE_PERMISSION_ID + "-999999",
                            auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    assert response.status_code == 404

