import pytest
import requests
import json
from common import credentials
from common import helpers


def test_edit_issue_edit_summary(test_issue):
    """editing summary of issue I have permission to"""
    """CAUTION: We are using pytest fixture here named test_issue."""
    """This fixture creates an issue before test for us so we have something to edit."""

    summary = "New summary"
    data = json.dumps({
        "fields": {
            "summary": summary
        }
    })
    response = requests.put(credentials.ISSUE_REST_URL + "/" + test_issue, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                            headers={'content-type': 'application/json'})
    assert response.status_code == 204
    assert response.text == ''
    """ TODO TASK1 Wykonaj dodatkowy request i sprawdź czy rzeczywiście summary zostało zmienione"""
