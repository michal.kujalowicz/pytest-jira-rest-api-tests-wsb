import pytest
import requests
from common import credentials

PROJECT_REST_URL = credentials.JIRA_TEST_INSTANCE_URL + "/rest/api/2/project"


def test_get_all_projects_unauthorised_no_project_returned():
    """requesting for all projects when unauthorised and no project is returned"""
    response = requests.get(PROJECT_REST_URL)
    """TODO wprowadź status code który podejrzewasz w linii poniżej"""
    assert response.status_code == 0


def test_get_all_projects_authorised_successful():
    """requesting for all projects when authorised and 1 project is returned"""
    response = requests.get(PROJECT_REST_URL, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    """TODO wprowadź status code który podejrzewasz w linii poniżej"""
    assert response.status_code == 0


def test_get_project_no_permission():
    """requesting for specific project and no permission"""
    project_url = PROJECT_REST_URL + "/" + credentials.PROJECT_NO_PERMISSION
    response = requests.get(project_url, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD))
    """TODO wprowadź status code który podejrzewasz w linii poniżej"""
    assert response.status_code == 0




