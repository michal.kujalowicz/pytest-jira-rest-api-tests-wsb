import requests


def test_hello_requests_jira():
    response = requests.get("https://jira.atlassian.com/rest/api/2/issue/JSWSERVER-1")
    assert response.status_code == 200
    assert response.headers['Content-Type'] == "application/json;charset=UTF-8"
    response_body = response.json()
    assert response_body["key"] == "JSWSERVER-1"
