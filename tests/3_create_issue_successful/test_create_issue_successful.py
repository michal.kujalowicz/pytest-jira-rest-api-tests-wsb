import pytest
import requests
import json
from common import credentials
from common import helpers

"""TODO TASK1 Przejdź przez test i upewnij się że rozumiesz każdą linię"""


def test_create_issue():
    """creating issue in project I have permission to"""

    project_id = credentials.PROJECT_WRITE_PERMISSION_ID
    issue_type_id = credentials.BUG_ISSUE_TYPE_ID

    """Alternative: dynamically finding ids of project and issue-type"""
    # project_id = helpers.get_project_id(credentials.PROJECT_WRITE_PERMISSION)
    # issue_type_id = helpers.get_issue_type_id("Bug")

    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": "This is a test issue",
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(credentials.ISSUE_REST_URL, data, auth=(credentials.JIRA_USER, credentials.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 201
    response_body = response.json()

    """ Cleanup - it is good practice to clean up after automated test"""
    issue_key = response_body['key']
    """ helpers.delete_issue is a helping function to delete issue that was created. It uses REST too"""
    helpers.delete_issue(issue_key)

