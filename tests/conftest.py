import pytest
from common import credentials
from common import helpers


@pytest.fixture(scope="module")
def test_issue():
    """
    Create test issue

    """
    issue_key = create_issue()
    yield issue_key
    helpers.delete_issue(issue_key)


@pytest.fixture(scope="module")
def test_issue_to_delete():
    """
    Create test issue but do not delete after

    """
    issue_key = create_issue()
    yield issue_key


def create_issue():
    response = helpers.create_issue(credentials.PROJECT_WRITE_PERMISSION_ID, "This is test issue", "This is test description", "Bug")
    response_body = response.json()
    issue_key = response_body['key']
    return issue_key
