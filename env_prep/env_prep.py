import requests
import pytest
import json
from common import helpers

def test_prepare_environment():

    create_set_user("AB")
    create_set_user("AC")
    create_set_user("AD")
    create_set_user("AE")
    create_set_user("AF")
    create_set_user("AG")
    create_set_user("AH")
    create_set_user("AI")
    create_set_user("AJ")
    create_set_user("AK")
    create_set_user("AL")
    create_set_user("AM")
    create_set_user("AN")
    create_set_user("AO")
    create_set_user("AP")
    create_set_user("AR")
    create_set_user("AS")
    create_set_user("AT")
    create_set_user("AU")
    create_set_user("AW")
    create_set_user("AX")
    create_set_user("AY")
    create_set_user("AZ")


def create_set_user(postfix):

    """creating users"""
    user = "user" + postfix
    project = "PROJ" + postfix
    password = "Test123!"
    response = helpers.create_user(user, "Test123!")
    assert response.status_code == 201
    response = helpers.create_project(project, user)
    assert response.status_code == 201
    response = helpers.get_project("PROJZZ")
    response_body = response.json()
    role_url = response_body["roles"]["Administrators"]
    role_id = helpers.get_project_role_id(role_url)
    response = helpers.assign_user_to_role(project, user, role_id)
    assert response.status_code == 200
    response = helpers.create_issue_by_user(project, "First issue in project", "First issue in project", "Bug",
                                            user, password)
    assert response.status_code == 201
